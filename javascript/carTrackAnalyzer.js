function TrackAnalyzer( trackData ){
	this.iData = trackData;
	
	if( 5 < GLOBAL.DEBUG_LEVEL ){
		for(var i in trackData.pieces ){
			GLOBAL.dbg(6,"Track p " + i + ":" + JSON.stringify(trackData.pieces[i], null, 4));
		}
	}
	this.tasks=[];
	this.iLaneProgram = [];
	this.iTurboMap = [];
	this.buildTrackMap(trackData);
	
	// holds the current car positions item
	this.iCarPositions = undefined;
	this.iRecordAcc = {mode:0,dstRatio:10, sampleSize:0};
		
}

TrackAnalyzer.prototype.buildTrackMapHelper = function ( data, currentItem ){

}

TrackAnalyzer.prototype.buildTrackMap = function(data){
	this.iMap = [];
	var currentItem = undefined;
	for( var i = 0; i < data.pieces.length; i++){
		if(data.pieces[i].length !== undefined ){
			// straight line
			if( currentItem === undefined ){
				currentItem = {t:0, l:data.pieces[i].length, start:i, end:i};
			}else{
				if( currentItem.t === 0 ){
					currentItem.l = currentItem.l + data.pieces[i].length;
					currentItem.end = i;
				}else{
					// finalize the task
					this.createNewTask(currentItem,data);
					currentItem = {t:0, l:data.pieces[i].length, start:i, end:i};
				}
			}
		}else{
			// a turn
			if( currentItem === undefined ){
				currentItem = {t:10, a:data.pieces[i].angle, r:data.pieces[i].radius , start:i, end:i};
			}else{
				if( currentItem.t === 10 ){
					// for turns it could be left or right turn
					if( (data.pieces[i].radius === currentItem.r ) &&
						((((data.pieces[i].angle > 0) && (currentItem.a > 0) ) ||
						((data.pieces[i].angle < 0) && (currentItem.a < 0) ) ) )){
							currentItem.a = currentItem.a + data.pieces[i].angle;
							currentItem.end = i;
						}else{
							this.createNewTask(currentItem,data);
							currentItem = {t:10, a:data.pieces[i].angle, r:data.pieces[i].radius , start:i, end:i};;						
						}
					currentItem.l = currentItem.l + data.pieces[i].length;
					currentItem.end = i;
				}else{
					// finalize the task
					this.createNewTask(currentItem,data);
					currentItem = {t:10, a:data.pieces[i].angle, r:data.pieces[i].radius , start:i, end:i};;
				}
			}			
		}
	}
	
	if( (currentItem.t === 0) && (this.iMap[0].t=== 0 )){
		this.iMap[0].l = currentItem.l + this.iMap[0].l;
		this.iMap[0].start = currentItem.start;
	}
	//this.createNewTask(currentItem,data);
	

	GLOBAL.dbg(1,JSON.stringify(this.iMap,null,4));
	for(var i in this.tasks){
		this.tasks[i].dbgPrint();
	}
	
	GLOBAL.dbg(1,"tasks - 2nd pass");

	for(var i=0; i<this.tasks.length; i++){
	//	console.log("2nd pass " + i);
		this.tasks[i].constant = false;
		var nextProgTask = null;
		var prevProgTask = null;
		if(  i === this.tasks.length - 1 ){
			nextProgTask = this.tasks[0];
		}else{
			nextProgTask = this.tasks[i+1];
		}
		
		if( i === 0 ){
			prevProgTask = this.tasks[this.tasks.length - 1];
		}else{
			prevProgTask = this.tasks[i-1];
		}
		
		if( (this.tasks[i].start.idx - prevProgTask.stop.idx) < 3 ){
			if(this.tasks[i].seg.r < 60 ){
				//this.tasks[i].data.maxa === this.tasks[i].data.maxa/2;
				//prevProgTask.data.maxa === prevProgTask.data.maxa/2;
				//prevProgTask.constant = true;
				this.tasks[i].constant = true;
				//vfix console.log("Setting constant for " + i);
			}
		}
/*			this.type = type; // 0 constant speed
	this.start = start;
	this.stop = stop;
	this.data = data;
	this.instance = null;
	this.seg = raw;
*/
	//	if( nextProgTask.
	}
	
	//GLOBAL.dbg(1,JSON.stringify(this.iLaneProgram,null,4));
	
	// we also want to print the lane change plan, lets do it for 20 loops, just to make sure :P
	var startPos = -1;
	for(vCkl = 0; vCkl < 20; vCkl++){
		for(var i = 0; i < data.pieces.length; i++){
			if(startPos === -1){
				startPos = i;
			}
			if( (startPos !== -1)&& (data.pieces[i]["switch"] === true )){
				var angle = this.getAngleToNextSwitch(data.pieces, i + 1 );
				//console.log("Next angle to push" + angle);
				this.iLaneProgram.push({p:startPos, l:(angle>0)?"Right":"Left", lap:vCkl});
				startPos = -1;
			}
		}
	}
	GLOBAL.dbg(1,JSON.stringify(this.iLaneProgram,null,4));
	
	// sorting turbo map
	var len = [];
	var tmpMap = [];
	for(var vCkl in this.iMap ){
		if( this.iMap[vCkl].t === 0 ){
			len.push(this.iMap[vCkl].l);
			tmpMap.push(this.iMap[vCkl]);
		}
	}
	len.sort(function(a,b){return b - a}) 
	
	for(var vCkl in len){
		var found = false;
		for(var i = 0; (i < tmpMap.length) && (found === false); i++){
			if( tmpMap[i].l === len[vCkl] ){
				this.iTurboMap.push(tmpMap[i]);
				tmpMap.splice(i,1);
				found = true;
			}
		}
	}
	GLOBAL.dbg(1,"Turbo map>" + JSON.stringify(this.iTurboMap,null,4));
}

TrackAnalyzer.prototype.getAngleToNextSwitch = function( pieces, idx ){
	var angle = 0;
	var isFound = false;
	for(var i = idx; ( i < pieces.length ) && ((!isFound)||(angle === 0)); i++ ){
		if( pieces[i].angle !== undefined ){
			angle = angle + pieces[i].angle;
			}
		if( pieces[i]["switch"] === true ){
			isFound = true;
		}
	}
	for(var i = 0; ( i < idx ) && ((!isFound)||(angle === 0)) ; i++ ){
		if( pieces[i].angle !== undefined ){
			angle = angle + pieces[i].angle;
		}
		if( pieces[i]["switch"] === true ){
			isFound = true;
		}
	}
	return angle;
}

TrackAnalyzer.prototype.createNewTask = function(seg,data){
	this.iMap.push(seg);
	var startPiece = seg.start;
	var endPiece = seg.end + 1;
	if(endPiece === data.pieces.length){
		endPiece = 0;
	}
	
	if(seg.t === 10){
		var maxAngle = 50;
		if( seg.radius < 60 ){
			maxAngle = 10;
		}else if( seg.radius < 70 ) {
			maxAngle = 30;
		}else if( seg.radius < 80 ) {
			maxAngle = 40;
		}
		if( seg.a > 0 ){
			// right turn
			this.tasks.push(new TaskInstance({idx:seg.start, pos:0},{idx:endPiece, pos:1}, 10,  {speed:2, maxa:maxAngle }, seg , this));
		}else{
			// left turn
			this.tasks.push(new TaskInstance({idx:seg.start, pos:0},{idx:endPiece, pos:1}, 10,  {speed:2, maxa:-maxAngle }, seg, this));
		}
	}
}

TrackAnalyzer.prototype.updateCarPositions = function(data){
	this.iCarPositions = data;
}

// this method is called by our own car and we can determine the breaking factor from this data over a period of some ticks
TrackAnalyzer.prototype.recordAcc = function(dst,acc, tick){
	if( acc < 0 ){
		if( this.iRecordAcc.mode === 0 ){
			// not recording yet, start recording
			this.iRecordAcc.mode = 10;
			this.iRecordAcc.startSpeed = dst;
			this.iRecordAcc.dst = dst;
			this.iRecordAcc.startTick = tick;
			this.iRecordAcc.endTick = tick;		
		}else{
			// continue recording
			this.iRecordAcc.endSpeed = dst;
			this.iRecordAcc.dst = this.iRecordAcc.dst + dst;
			this.iRecordAcc.endTick = tick;			
		}
	}else{
		// check, if we have been recording, then its time to update the data
		if( this.iRecordAcc.mode !== 0 ){
			this.iRecordAcc.mode = 0;
			var newDstRatio = this.iRecordAcc.dst/(this.iRecordAcc.startSpeed - this.iRecordAcc.endSpeed);
			var newSampleSize =  this.iRecordAcc.endTick - this.iRecordAcc.startTick + 1;
			if( newSampleSize > this.iRecordAcc.sampleSize ){
				this.iRecordAcc.dstRatio = newDstRatio;
				this.iRecordAcc.sampleSize = newSampleSize;
			}else if( newSampleSize === this.iRecordAcc.sampleSize ){
				this.iRecordAcc.dstRatio = Math.max(this.iRecordAcc.dstRatio, newDstRatio );
			}
		}
	}
}



// returns distance between 
// note, that in order to measure the whole track, end segment should be different then start segment. 
TrackAnalyzer.prototype.diffDistance = function(start, end, lanes){
	//console.log("DD: " + JSON.stringify(start,null,4) + "\n" + JSON.stringify(end,null,4) + "\n"+JSON.stringify(lanes,null,4) );
	var distance = 0;
	if( (start === undefined) || (end === undefined )){
		//bad params
		return 0;
	}
	var curIdx = start.piecePosition.pieceIndex;
	while(curIdx !== end.piecePosition.pieceIndex){
		var lanevalue = lanes[curIdx - start.piecePosition.pieceIndex];
		if( undefined === lanevalue ){
			lanevalue = 0;
		}
		distance = distance + this.calcPieceLength(curIdx, lanevalue);
		curIdx++;
		if( curIdx ===  this.iData.pieces.length){
			curIdx = 0;
		}
	}
	distance = distance - start.piecePosition.inPieceDistance + end.piecePosition.inPieceDistance;
	//console.log("Returning " + distance);
	return distance;
}

// returns distance between 
// note, that in order to measure the whole track, end segment should be different then start segment. 
TrackAnalyzer.prototype.diffDistanceSameLane = function(start, end, lane){
	var distance = 0;
	if( (start === undefined) || (end === undefined )){
		//GLOBAL.dbg(1,"bad params: diffDistance returns 0");
		return 0;
	}
	var curIdx = start.piecePosition.pieceIndex;
	while(curIdx !== end.piecePosition.pieceIndex){
		var lanevalue = lane;
		distance = distance + this.calcPieceLength(curIdx, lanevalue);
		curIdx++;
		if( curIdx ===  this.iData.pieces.length){
			curIdx = 0;
		}
	}
	distance = distance - start.piecePosition.inPieceDistance + end.piecePosition.inPieceDistance;
	return distance;
}


// calculates the length of a piece, taking lane into account
TrackAnalyzer.prototype.calcPieceLength = function( idx /* piece index */, lane /* lane idx */){
	// no sanity checks. We don't want extra  processing here, since it will be called alot.
	var seg = this.iData.pieces[idx];
	if( seg.length !== undefined ){
		// its a straight segment return the length
		return seg.length;
	}else if(seg.radius !== undefined){
		var delta = this.iData.lanes[lane].distanceFromCenter;
		if( seg.angle > 0 ){
			delta = -delta;
		}
		var radius = seg.radius + delta;
		return Math.abs((radius*seg.angle*Math.PI/180));
	}else{
		return 0;
	}
}


function TaskInstance( start, stop , type, data, raw, an ){
	this.type = type; // 0 constant speed
	this.start = start;
	this.stop = stop;
	this.data = data;
	this.instance = null;
	this.seg = raw;
	this.iAnalyzer = an;
	
	switch(type){
		case 0:
			this.instance = new TaskConstantSpeed(this);
		break;
		case 10:
			this.instance = new TaskVRTurn(this);
		break;

	}
}

TaskInstance.prototype.dbgPrint = function(){
	GLOBAL.dbg(3, "Task : " + JSON.stringify({type:this.type, start:this.start, end: this.stop, data:this.data},null,4));
}

TaskInstance.prototype.newRun = function(){
	this.instance.newRun(); // called once the task becomes current again
}

TaskInstance.prototype.taskCompleted = function(analyzer){
	// NOOP
}

TaskInstance.prototype.isCurrent = function( piecePosition ){
	if( 
		(( piecePosition.pieceIndex > this.start.idx ) &&  ( piecePosition.pieceIndex < this.stop.idx ))||
		(( piecePosition.pieceIndex === this.start.idx ) &&  ( piecePosition.inPieceDistance >= this.start.pos ))||
		(( piecePosition.pieceIndex === this.stop.idx ) &&  ( piecePosition.inPieceDistance < this.stop.pos ))
		){
			return true;
		}
	return false;
}

TaskInstance.prototype.getNextThrottle = function( blackBox, tick, analyzer){
	return this.instance.getNextThrottle(blackBox, tick, analyzer);
}

function TaskConstantSpeed( data ){
	this.parent = data;
}

TaskConstantSpeed.prototype.newRun = function(){
	// do nothing
}


TaskConstantSpeed.prototype.getNextThrottle = function ( blackBox, tick, analyzer ){
	var aThrottle = 0;
	if(blackBox.dpt[tick] < this.parent.data.speed ){
		aThrottle = 1;
	}else{
		aThrottle = 0;
	}
	return aThrottle;
}

function TaskVRTurn( data ){
	this.parent = data;
	this.iRunCount=0;
	this.iAvgUpdated = false;
	this.iZeroAngleStatus = 0; // new run, 10 - was 0, 20 was not 0 
	this.iCount = 0;
}

TaskVRTurn.prototype.newRun = function(){
	// reset measurement data
	this.iAverage = 0;
	this.iCount = 0;
	this.iMaxChange = 0;
	this.iSpeedChange = this.parent.data.speed;
	this.iEntry = this.parent.data.speed;
	this.iRunCount++;
}


TaskVRTurn.prototype.getNextThrottle = function ( blackBox, tick, analyzer ){
	var aThrottle = 0;
	// get distance till task end
	var diff = analyzer.diffDistanceSameLane(blackBox.raw[tick], {piecePosition:{pieceIndex:this.parent.stop.idx, inPieceDistance: this.parent.stop.pos}} ,blackBox.raw[tick].piecePosition.lane.endLaneIndex);
	if(blackBox.dpt[tick] === 0){
		// apparently we have crashed, kick-start
			aThrottle = 1;
			return aThrottle;
	}
	// consider the turbo mode
	var slowFactor = 10;
	if( (tick > this.parent.iAnalyzer.iTurboOn) && (tick < this.parent.iAnalyzer.iTurboOff) ){
		// torbo is on, so we will accelerate much faster!take it into account
		//sf slowFactor = slowFactor * this.iTurboFactor;
		slowFactor = slowFactor;
		
	}
	  
	//var expected_rate = (this.parent.data.maxa - slowFactor*Math.abs(blackBox.angleChange[tick]) - blackBox.angle[tick])/(diff/blackBox.dpt[tick]);
	//console.log("expected ac rate>" + expected_rate);
	var expected_rate = (this.parent.data.maxa - slowFactor*blackBox.angleChange[tick] - blackBox.angle[tick])/(diff/blackBox.dpt[tick]);
	// we've got to take into account the turn direction
	if( this.parent.data.maxa  > 0 ){
		// right turn
		if( expected_rate > blackBox.angleChange[tick] ){
		//	console.log("GO FASTER_____________________________");
			aThrottle = 1;

			if(this.parent.constant === true){
				aThrottle = 1 - Math.abs((blackBox.angle[tick] / this.parent.data.maxa));
				if((((this.parent.stop.idx- this.parent.start.idx)<=1)&&(Math.abs(this.parent.seg.a)>=45))||(((this.parent.stop.idx- this.parent.start.idx)<=2)&&(Math.abs(this.parent.seg.a)>=90))){
					if( aThrottle < 0.2 ){
						aThrottle = 0.2;
					}					
				}else{
					if( aThrottle < 0.6 ){
						aThrottle = 0.6;
					}
				}
			}
	if( (tick > this.parent.iAnalyzer.iTurboOn) && (tick < this.parent.iAnalyzer.iTurboOff) ){
			aThrottle = aThrottle / this.parent.iAnalyzer.iTurboFactor;
			//aThrottle = 0;
	}
		}else{
		//	console.log("GO SLOWER_____________________________");
			aThrottle = 0;
		}
	// updating next entry speed
		this.iAverage = (this.iAverage * this.iCount + blackBox.dpt[tick])/(this.iCount + 1);
		this.iCount++;
		
		if( this.iMaxChange < blackBox.angle[tick] ){
			this.iMaxChange = blackBox.angle[tick] ;
			this.iSpeedChange = blackBox.dpt[tick] ;
		}

	}else{
		// its a left turn
		if( expected_rate > blackBox.angleChange[tick] ){
			//console.log("GO SLOWER_____________________________");
			aThrottle = 0;
		}else{
			//	console.log("GO FASTER_____________________________");
			aThrottle = 1;
			if(this.parent.constant === true){
				aThrottle = 1 - Math.abs((blackBox.angle[tick] / this.parent.data.maxa));
				if((((this.parent.stop.idx- this.parent.start.idx)<=1)&&(Math.abs(this.parent.seg.a)>=45))||(((this.parent.stop.idx- this.parent.start.idx)<=2)&&(Math.abs(this.parent.seg.a)>=90))){
					if( aThrottle < 0.2 ){
						aThrottle = 0.2;
					}					
				}else{
					if( aThrottle < 0.6 ){
						aThrottle = 0.6;
					}
				}
			}
	if( (tick > this.parent.iAnalyzer.iTurboOn) && (tick < this.parent.iAnalyzer.iTurboOff) ){
			aThrottle = aThrottle / this.parent.iAnalyzer.iTurboFactor;
			//aThrottle = 0;
	}	
		}

		this.iAverage = (this.iAverage * this.iCount + blackBox.dpt[tick])/(this.iCount + 1);
		this.iCount++;
		//this.parent.data.speed = this.iAverage;
		if( this.iMaxChange > blackBox.angle[tick] ){
			this.iMaxChange = blackBox.angle[tick] ;
			this.iSpeedChange = blackBox.dpt[tick] ;
		}

	}
	var oldSpeed = this.parent.data.speed;
	this.parent.data.speed = this.iAverage;	
	// trying to increase learning gain
	
	// we want to skip the first tick, due to possible jumps on speed measurements.
	//console.log(">" + this.iRunCount + ";" + this.iCount + ";" + blackBox.angle[tick] + ";" + this.iZeroAngleStatus);
	if((this.iRunCount === 1)&&(this.iCount>1)){
		if(this.iZeroAngleStatus === 0 ){
			if( blackBox.angle[tick] === 0 ){
				this.iZeroAngleStatus = 10;
			}else{
				this.iZeroAngleStatus = 20;
			}
		}else if( this.iZeroAngleStatus === 10 ){
			if( blackBox.angle[tick] !== 0 ){
				// do the no-sliding update here
				for(var vCkl in analyzer.tasks){
		//			console.log("TT " + analyzer.tasks[vCkl].instance.iRunCount + ";"+ analyzer.tasks[vCkl].instance.iZeroAngleStatus);
					if( ( analyzer.tasks[vCkl].instance.iRunCount === 0 ) && (analyzer.tasks[vCkl].instance.iZeroAngleStatus === 0)){
						//console.log("Updating initial speed for " + analyzer.tasks[vCkl].seg.start );
						analyzer.tasks[vCkl].data.speed = analyzer.getNoslipSpeed(this.parent.seg.r,blackBox.dpt[tick],  blackBox.angle[tick],analyzer.tasks[vCkl].seg.r );
						if(analyzer.tasks[vCkl].seg.r < 60){
							analyzer.tasks[vCkl].data.speed = analyzer.tasks[vCkl].data.speed * 0.6;
						}
						//console.log("   radius " + blackBox.angle[tick],analyzer.tasks[vCkl].seg.r );
						//console.log("    set to " + analyzer.tasks[vCkl].data.speed );
						analyzer.tasks[vCkl].instance.iZeroAngleStatus = 30;
					}
				}
				this.iZeroAngleStatus = 20;		
			}
		}
	}
	/*
	 if((this.iZeroAngleStatus === 30) && (this.parent.seg.r < 60 ) && ((this.parent.stop.idx- this.parent.start.idx)===0)&&(Math.abs(this.parent.seg.a)>=45)){
		if( this.parent.constant === true ){ 
			console.log("reset speed for " + this.parent.seg.a);
			this.parent.data.speed = oldSpeed;
		}
	} 
	*/
	 if((this.iZeroAngleStatus === 30) /*&& (this.parent.seg.r < 60 ) */&& (((this.parent.stop.idx- this.parent.start.idx)<=1)&&(Math.abs(this.parent.seg.a)>=45))||(((this.parent.stop.idx- this.parent.start.idx)<=2)&&(Math.abs(this.parent.seg.a)>=90)))
	 {
			// RESETING max angle
			if( this.parent.data.maxa > 0 ){
				this.parent.data.maxa = 20;
				}else{
				this.parent.data.maxa = -20;
				}
				
				if(((this.parent.stop.idx- this.parent.start.idx)>=2)&&(Math.abs(this.parent.seg.r)>=50)){
					if( this.parent.data.maxa > 0 ){
						this.parent.data.maxa = 5;
						}else{
						this.parent.data.maxa = -5;
						}
				}
				

			//vfix console.log("reset speed for " + this.parent.seg.a);
			//this.parent.data.speed=oldSpeed+(this.parent.data.speed-oldSpeed)*(1-Math.abs(this.iMaxChange)/Math.abs(this.parent.data.maxa));
				if( (Math.abs(this.iMaxChange)/Math.abs(this.parent.data.maxa)) < 1 ){
		//			console.log("+++++SET "+tick+":"+this.iMaxChange+":"+this.parent.data.maxa+":"+(Math.abs(this.iMaxChange)/Math.abs(this.parent.data.maxa))+":"+this.parent.data.speed);
					//	this.parent.data.speed = oldSpeed;
				}else{
	//				console.log("-----RESET "+tick+":"+this.iMaxChange+":"+this.parent.data.maxa+":"+(Math.abs(this.iMaxChange)/Math.abs(this.parent.data.maxa))+":"+this.parent.data.speed);
			//if(this.parent.seg.r<100){
					this.parent.data.speed = oldSpeed;
			/*}else{
					this.parent.data.speed = this.parent.data.speed-1;			
			}*/
				}
			/*if( Math.abs(this.iMaxChange) < Math.abs(this.parent.data.maxa)/3 ){
				this.parent.data.speed = (this.parent.data.speed + 3*oldSpeed)/4;
			}else{
				this.parent.data.speed = oldSpeed;
			}*/
	}else{
			//vfix console.log("NO reset speed for " + this.parent.seg.a+ " idx " + (this.parent.stop.idx- this.parent.start.idx) + " r: " + this.parent.seg.r);

	}	
	
	if( blackBox.dpt[tick] < 3 ){
		aThrottle = 0.5;
	}
		/*if(this.parent.constant === true){
		 this.parent.data.speed = oldSpeed;
	}*/
	
	//aThrottle = 1- (blackBox.angle[tick]/this.parent.data.maxa);
	//console.log(""+tick+":"+aThrottle+ " on "+ this.parent.iAnalyzer.iTurboOn + " off " + this.parent.iAnalyzer.iTurboOff + " f "+ this.parent.iAnalyzer.iTurboFactor);
	return aThrottle;
}



TrackAnalyzer.prototype.getNoslipSpeed = function( r_in, speed, slide_a, r_next){
		// r_in expected to be grater the 0
		var Bang = this.radToDeg(speed/r_in);
		var Bslip = Bang - slide_a;
		var Vmax = this.degToRad(Bslip)*r_in;
		var Fslip = Math.pow(Vmax,2)/r_in;
		
		return Math.sqrt(Fslip*r_next);
}

TrackAnalyzer.prototype.getCurrentTask = function(piece){
	var search={d:0, i:-1};

	if( (this.currentTask !== undefined)&& (this.currentTask.isCurrent(piece))){
		// do nothing
	}else{
		if(this.currentTask !== undefined ){
			this.currentTask.taskCompleted(this);
		}
		this.currentTask = undefined;
		for(var i=0; i<this.tasks.length;i++){
			if(this.tasks[i].isCurrent(piece)){
				this.currentTask =  this.tasks[i];
				if( i === (this.tasks.length - 1) ){
					this.nextTask = this.tasks[0];
				}else{
					this.nextTask = this.tasks[i+1];
				}
			}else{
				var tmpData = {piecePosition:{pieceIndex:this.tasks[i].start.idx, inPieceDistance:this.tasks[i].start.pos}};
				var diff = this.diffDistance({piecePosition:piece}, 
				tmpData, []);
				if( search.i === -1 ){
					search.i = i;
					search.d = diff;  
				}else{
					if( search.d > diff ){
						search.i = i;
						search.d = diff;  					
					}
				}
			}
		}
		if(this.currentTask !== undefined ){
			this.currentTask.newRun();
		}
	}
	if( ( this.nextTask === undefined) && (search.i !== -1 ) ){
		this.nextTask = this.tasks[search.i];
	}
	if( this.nextTask === this.currentTask ){
		this.nextTask = undefined;
	}
	return this.currentTask;
}


TrackAnalyzer.prototype.getAngle = function(start,stop ){
	this.iData;
	var angle = this.iData.pieces[start.idx].angle - (start.pos*180)/(Math.PI*this.iData.pieces[start.idx].radius);
	for(var i = start.idx+1; i < stop.idx; i++){
		if( this.iData.pieces[i].angle !== undefined ){
		
			angle = angle + this.iData.pieces[i].angle;
		}
	}

	if( this.iData.pieces[stop.idx].angle !== undefined ){
		if( start.idx !== stop.idx ){
			angle = angle + (stop.pos*180)/(Math.PI*this.iData.pieces[stop.idx].radius);
		}else{
			angle = ((stop.pos-start.pos)*180)/(Math.PI*this.iData.pieces[stop.idx].radius);
		}
	}
	
	return angle;
}
TrackAnalyzer.prototype.toRad = function (angle) {
	return Math.PI*angle/180;
}

TrackAnalyzer.prototype.radToDeg = function (angle) {
	return angle*180/Math.PI;
}

TrackAnalyzer.prototype.degToRad = function (angle) {
	return Math.PI*angle/180;
}


TrackAnalyzer.prototype.posToAngle = function (pos, R) {
	return (180*pos)/(Math.PI*R);
}


TrackAnalyzer.prototype.angleToPos = function (angle, R) {
	return (Math.PI*R)*angle/180;
}


exports.TrackAnalyzer = TrackAnalyzer;