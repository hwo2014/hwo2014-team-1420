var testTrackData = {
                "id": "keimola",
                "name": "Keimola",
                "pieces": [
                    {
                        "length": 100
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100,
                        "switch": true
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 200,
                        "angle": 22.5,
                        "switch": true
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100
                    },
                    {
                        "radius": 200,
                        "angle": -22.5
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100,
                        "switch": true
                    },
                    {
                        "radius": 100,
                        "angle": -45
                    },
                    {
                        "radius": 100,
                        "angle": -45
                    },
                    {
                        "radius": 100,
                        "angle": -45
                    },
                    {
                        "radius": 100,
                        "angle": -45
                    },
                    {
                        "length": 100,
                        "switch": true
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 200,
                        "angle": 22.5
                    },
                    {
                        "radius": 200,
                        "angle": -22.5
                    },
                    {
                        "length": 100,
                        "switch": true
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "length": 62
                    },
                    {
                        "radius": 100,
                        "angle": -45,
                        "switch": true
                    },
                    {
                        "radius": 100,
                        "angle": -45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "radius": 100,
                        "angle": 45
                    },
                    {
                        "length": 100,
                        "switch": true
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 100
                    },
                    {
                        "length": 90
                    }
                ],
                "lanes": [
                    {
                        "distanceFromCenter": -10,
                        "index": 0
                    },
                    {
                        "distanceFromCenter": 10,
                        "index": 1
                    }
                ],
                "startingPoint": {
                    "position": {
                        "x": -300,
                        "y": -44
                    },
                    "angle": 90
                }
            }
 
 var testTrackMap = [
    {
        "start": "0",
        "length": 400,
        "end": "3"
    },
    {
        "start": "4",
        "angle": 202.5,
        "end": "8"
    },
    {
        "start": "9",
        "length": 200,
        "end": "10"
    },
    {
        "start": "11",
        "angle": -22.5,
        "end": "11"
    },
    {
        "start": "12",
        "length": 200,
        "end": "13"
    },
    {
        "start": "14",
        "angle": -180,
        "end": "17"
    },
    {
        "start": "18",
        "length": 100,
        "end": "18"
    },
    {
        "start": "19",
        "angle": 202.5,
        "end": "23"
    },
    {
        "start": "24",
        "angle": -22.5,
        "end": "24"
    },
    {
        "start": "25",
        "length": 100,
        "end": "25"
    },
    {
        "start": "26",
        "angle": 90,
        "end": "27"
    },
    {
        "start": "28",
        "length": 62,
        "end": "28"
    },
    {
        "start": "29",
        "angle": -90,
        "end": "30"
    },
    {
        "start": "31",
        "angle": 180,
        "end": "34"
    },
    {
        "start": "35",
        "length": 490,
        "end": "39"
    }
];
 
 exports.testTrackData = testTrackData;
 exports.testTrackMap = testTrackMap;