var mTrackAnalyzer = require("../carTrackAnalyzer.js");
var mTestData = require("./test_track_data.js");

function trackAnalyzerTest_calcPieceLength_straight( /*TrackAnalyzer */obj){
	var result = obj.calcPieceLength(0,0);
	if( result !== 100 ){
		return false;
	};
	
	return true;
}


function trackAnalyzerTest_calcPieceLength_arcPositive_outerLane( /*TrackAnalyzer */obj){
	var result = obj.calcPieceLength(4,0);
	/*
	                    {
                        "radius": 100,
                        "angle": 45
                    },
	*/
	if( result !== 86.39379797371932 ){
	console.log("calcPieceLength_arcPositive_outterLane " + result);
		return false;
	};
	
	return true;
}

function trackAnalyzerTest_calcPieceLength_arcPositive_innerLane( /*TrackAnalyzer */obj){
	var result = obj.calcPieceLength(4,1);
	if( result !== 70.68583470577035 ){
		console.log("calcPieceLength_arcPositive_innerLane " + result);
		return false;
	};
	
	return true;
}


function trackAnalyzerTest_calcPieceLength_arcNegative_outerLane( /*TrackAnalyzer */obj){
	/*
	                    {
                        "radius": 200,
                        "angle": -22.5
                    },

	*/
	var result = obj.calcPieceLength(11,1);
	if( result !== 82.46680715673207 ){
		console.log("calcPieceLength_arcNegative_outterLane " + result);
		return false;
	};
	
	return true;
}

function trackAnalyzerTest_calcPieceLength_arcNegative_innerLane( /*TrackAnalyzer */obj){
	var result = obj.calcPieceLength(11,0);
	if( result !== 74.61282552275757 ){
		console.log("calcPieceLength_arcNegative_innerLane " + result);
		return false;
	};
	
	return true;
}


function trackAnalyzerTest_diffDistance_samePiece( /*TrackAnalyzer */obj){
	var testPos1 = {"piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 1,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }};

	var testPos2 = {"piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 14.234259192561145,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }};
				
	var diffResult = obj.diffDistance(testPos1, testPos2, [0] /* lane 0 */);
	if( diffResult !== 13.234259192561145 ){
		return false;
	};
	
	return true;
};


function trackAnalyzerTest_diffDistance_raceProblem( /*TrackAnalyzer */obj){
	var testPos1 = {
    "id": {
        "name": "The Flight of Dodo",
        "color": "yellow"
    },
    "angle": 0.1354948932514,
    "piecePosition": {
        "pieceIndex": 13,
        "inPieceDistance": 92.6831933517294,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 0
        },
        "lap": 8
    }
};

	var testPos2 =  { "id": {
        "name": "The Flight of Dodo",
        "color": "yellow"
    },
    "angle": 0.13196602133769286,
    "piecePosition": {
        "pieceIndex": 13,
        "inPieceDistance": 94.15074938439257,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 0
        },
        "lap": 8
    }
};
				
	var diffResult = obj.diffDistance(testPos1, testPos2, [0] /* lane 0 */);
	console.log("TestX diffResult " + diffResult);
	if( diffResult !== 13.234259192561145 ){
		return false;
	};
	
	return true;
};


function trackAnalyzerTest_diffDistance_diffPieceDiffLane( /*TrackAnalyzer */obj){
	var testPos1 = {"piecePosition": {
        "pieceIndex": 17,
        "inPieceDistance": 64.7593996999083,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 0
        },
        "lap": 0
    }};

	var testPos2 = { "piecePosition": {
        "pieceIndex": 18,
        "inPieceDistance": 0.5278050868905951,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 1
        },
        "lap": 0
    } };
				
	var diffResult = obj.diffDistance(testPos1, testPos2, [0,1] /* lane 0 */);
	if( diffResult !== 6.4542400927526415 ){
		return false;
	};
	
	return true;
};


function trackAnalyzerTest_diffDistance_thirdPiece( /*TrackAnalyzer */obj){
	var testPos1 = {"piecePosition": {
                    "pieceIndex": 0,
                    "inPieceDistance": 1,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }};

	var testPos2 = {"piecePosition": {
                    "pieceIndex": 2,
                    "inPieceDistance": 14.234259192561145,
                    "lane": {
                        "startLaneIndex": 0,
                        "endLaneIndex": 0
                    },
                    "lap": 0
                }};
				
	var diffResult = obj.diffDistance(testPos1, testPos2, [0,0,0] /* lane 0 */);
	if( diffResult !== 213.234259192561145 ){
		console.log("calcPieceLength_diffDistance_thirdPiece " + diffResult);
		return false;
	};
	
	return true;
};

function trackAnalyzerTest_diffDistance_samePiece2( /*TrackAnalyzer */obj){
	var testPos1 = {"piecePosition": {
        "pieceIndex": 31,
        "inPieceDistance": 81.3569561284952,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 0
        },
        "lap": 0
    }};

	var testPos2 = {"piecePosition": {
        "pieceIndex": 32,
        "inPieceDistance": 1.4617739655900976,
        "lane": {
            "startLaneIndex": 0,
            "endLaneIndex": 0
        },
        "lap": 0
    }};
				
	var diffResult = obj.diffDistance(testPos1, testPos2, [0,0] /* lane 1 */);
	if( diffResult !== 6.498615810814215  ){
		console.log("calcPieceLength_diffDistance_thirdPiece2 " + diffResult);
		console.log("31>"+JSON.stringify(obj.iData.pieces[31], null, 4));
		console.log("32>"+JSON.stringify(obj.iData.pieces[32], null, 4));
		return false;
	};
	
	return true;
};


function trackAnalyzerTest_calculateAngleFull( /*TrackAnalyzer */obj){
	var result = obj.getAngle({idx:4,pos: 0},{idx: 8, pos:0});
	console.log("angle: " + result);
/*	if( result !== 74.61282552275757 ){
		console.log("calcPieceLength_arcNegative_innerLane " + result);
		return false;
	};
	*/
	if( result !== 180 ){
		return false;
		}
		return true;
}

function trackAnalyzerTest_calculateAngleHalfStart( /*TrackAnalyzer */obj){
	var result = obj.getAngle({idx:4,pos: obj.angleToPos(22.5, 100)},{idx: 7, pos:obj.angleToPos(22.5, 100)});
	console.log("anglex: " + result);
/*	if( result !== 74.61282552275757 ){
		console.log("calcPieceLength_arcNegative_innerLane " + result);
		return false;
	};
	*/
	if( result !== 135 ){
		return false;
		}
		return true;
}

function trackAnalyzerTest_noslip( /*TrackAnalyzer */obj){
	var result = obj.getNoslipSpeed(110,7,0.45,90);
	console.log("noslipspeed: " + result);
	if( result !== 5.550276 ){
		return false;
		}
		return true;
}


/*
function dstAccHelper( dst, obj, speed, acc, tick){
	var newDst = (dst + speed*(tick - obj.iCurrentSMTick));
	obj.updateSpeedMap(speed, acc, tick);
	
	//console.log("DstAcc: " + newDst);
	return newDst;
}
*/
exports.Suit = 
{
	run:function(data){
		var overalResult = true;
		var result;
		
		var testObj = new mTrackAnalyzer.TrackAnalyzer( data );

		// verify that we have constructed map properly
/*		for(var i in mTestData.testTrackMap){
			if( !testObj.compareMapItem(i, mTestData.testTrackMap[i]) ){
				ovaralResult = false;
				console.log("Map Data doesn't match at index " + i);
			}
		}
	*/	
		result = trackAnalyzerTest_calcPieceLength_straight(testObj);
		console.log("trackAnalyzerTest_calcPieceLength_straight: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_calcPieceLength_arcPositive_outerLane(testObj);
		console.log("trackAnalyzerTest_calcPieceLength_arcPositive_outerLane: " + result);
		overalResult = overalResult && result;
		
		result = trackAnalyzerTest_calcPieceLength_arcPositive_innerLane(testObj);
		console.log("trackAnalyzerTest_calcPieceLength_arcPositive_innerLane: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_calcPieceLength_arcNegative_outerLane(testObj);
		console.log("trackAnalyzerTest_calcPieceLength_arcNegative_outerLane: " + result);
		overalResult = overalResult && result;
		
		result = trackAnalyzerTest_calcPieceLength_arcNegative_innerLane(testObj);
		console.log("trackAnalyzerTest_calcPieceLength_arcNegtive_innerLane: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_diffDistance_samePiece(testObj);
		console.log("trackAnalyzerTest_diffDistance_samePiece: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_diffDistance_diffPieceDiffLane(testObj);
		console.log("trackAnalyzerTest_diffDistance_diffPieceDiffLane: " + result);
		overalResult = overalResult && result;

		
		result = trackAnalyzerTest_diffDistance_thirdPiece(testObj);
		console.log("trackAnalyzerTest_diffDistance_thirdPiece: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_diffDistance_samePiece2(testObj);
		console.log("trackAnalyzerTest_diffDistance_samePiece2: " + result);
		overalResult = overalResult && result;

		result = trackAnalyzerTest_calculateAngleFull(testObj);
		console.log("trackAnalyzerTest_calculateAngleFull: " + result);
		overalResult = overalResult && result;
	
		result = trackAnalyzerTest_calculateAngleHalfStart(testObj);
		console.log("trackAnalyzerTest_calculateAngleHalfStart: " + result);
		overalResult = overalResult && result;
		
/*		result = trackAnalyzerTest_diffDistance_raceProblem(testObj);
		console.log("trackAnalyzerTest_diffDistance_raceProblem: " + result);
		overalResult = overalResult && result;
*/
		result = trackAnalyzerTest_noslip(testObj);
		console.log("trackAnalyzerTest_noslip: " + result);
		overalResult = overalResult && result;
		
		return overalResult;
	}
};