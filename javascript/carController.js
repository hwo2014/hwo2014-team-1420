var mTrackAnalyzer = require("./carTrackAnalyzer.js");
var mSingleCar = require("./carSingleCar.js");

function carController(){
	GLOBAL.dbg(1,"Creating car controller instance. Note, initialization is required before use.");
}

carController.prototype.init = function( data, stat ){
	GLOBAL.dbg(1,"Initializing controller object...");
	
	// setting a link to the statistics object
	this.iStamp = 0;
	this.iIPTime = 0;
	this.iIPCounter = 0;
	
	// race data vars
	this.iPieces = null;
	this.iLanes = null;
	this.iCars = null;
	this.iRaceSession = null;
	this.iTrackAnalyzer = null;
	this.iThrottle = 0;
	this.iTurbo = [];
	this.turboTick = 0;
	
	this.raceStage = 0;
	this.onTrack = true;
	// minor statistics
	this.statData = {raw:[], counters:{}};
	GLOBAL.dbg(1,"Done");
}

carController.prototype.soi = function( data ){
	this.givenTick = undefined;
	// processing messages from server
	/*this.iStamp = (new Date()).getTime();
	if( GLOBAL.DEBUG_LEVEL > 5 ){
		this.iStamp = (new Date()).getTime();
	}*/
	
	// collecting data for debugging
	//*********************************************************************************
	this.statData.raw.push({t:data.gameTick, d:data.data });
	if( this.statData.counters[data.msgType] === undefined ){
		this.statData.counters[data.msgType] = 0;
	}
	this.statData.counters[data.msgType]++;
	// ********************** end of stat collecting **********************************
	
	if( "carPositions" === data.msgType ){
		// time to update the car data
		this.givenTick = data.gametTick;
		//console.log("============================================" +data.gameTick /*+"\n" + JSON.stringify(data.data,null,4)*/);
		this.iTrackAnalyzer.updateCarPositions(data.data, data.gametTick);
		var carData = null;
		for(var vCkl in data.data ){
			if( data.data[vCkl].id.color === this.iCar.id.color){
			carData = data.data[vCkl];
				this.iCar.ctl.updateCar(carData,data.gameTick, this.iTrackAnalyzer);
			}
		}
		this.updateThrottle(carData, data.gameTick);
	}else if( "gameInit" === data.msgType ){
		this.iThrottle = 0;
		this.iTurbo = [];
		this.turboTick = 0;

			if(this.raceStage === 10 ){
				this.saveQBox = this.iCar.ctl.blackBox;
				this.saveQA = this.iTrackAnalyzer;
			}
				// this is track data, lets build the track 
				this.iPieces = data.data.race.track.pieces;
				this.iLanes = data.data.race.track.lanes;
				this.iCars = data.data.race.cars;
				// perhaps iPieces and iLanes are redundant 
				this.iTrackAnalyzer = new mTrackAnalyzer.TrackAnalyzer( data.data.race.track );
				this.firstRun = true;
				if(this.raceStage === 10){
					this.iTrackAnalyzer = this.saveQA;
					this.firstRun = false;
				}
				this.raceStage = 10;
				for( var vCkl in this.iCars ){
					if(this.iCars[vCkl].id.color === this.iCar.id.color){
						this.iCar.ctl = new mSingleCar.SingleCar(this.iCars[vCkl]);
					}
				}
						
		this.iRaceSession = data.data.race.raceSession;
		// printing out track info
		GLOBAL.dbg(2,"Number of cars is " + this.iCars.length);
		GLOBAL.dbg(2,"Session data " + JSON.stringify(this.iRaceSession, null, 4));
	}else if("yourCar" === data.msgType ){
		this.iCar = {id:data.data};
		GLOBAL.dbg(2,"Car is: " + JSON.stringify(this.iCar, null, 4));
	}else if( data.msgType == "tournamentEnd" ){
		GLOBAL.dbg(2,"RACEBOX=========================================================");
		this.iCar.ctl.dumpBlackBox();

		if( this.saveQBox !== undefined ){
			GLOBAL.dbg(2,"QBOX============================================================");
			this.iCar.ctl.blackBox = this.saveQBox;
			this.iCar.ctl.dumpBlackBox();
			}
		for( var v in this.iTrackAnalyzer.tasks ){
			this.iTrackAnalyzer.tasks[v].dbgPrint();
		}
		GLOBAL.dbg(2,this.statData.counters);
	}else if("turboAvailable" === data.msgType){
		if( this.onTrack === true ){
			//console.log("************************************************TURBO GIVEN");
			// VFIX turbo seems to be broken ! this.iTurbo.push(data.data);
			this.iTurbo.push(data.data);
			}else{
				//console.log("*************** TURBO MISSED! ");
			}
	}else if( data.msgType === "crash" ){
		console.log("CRASH " + JSON.stringify(data));
		GLOBAL.dbg(2,"CRASH " + JSON.stringify(data));
		if( data.data.color === this.iCar.id.color ){
			this.onTrack = false;
			// we have crashed, reset the turbos
			// and probably do other updates later
			//console.log("Resetting turbo queue");
			// clearing the turbo just  in case
			this.iTurbo = [];
		}
	}else if( data.msgType === "spawn" ){
		GLOBAL.dbg(2,"SPAWN " + JSON.stringify(data));
		if( data.data.color === this.iCar.id.color ){
			this.onTrack = true;
		}
	}else if( data.msgType === "dnf" ){
		if( data.data.color === this.iCar.id.color ){
			//console.log("dnf " + JSON.stringify(data));
		}
	}
}
// end of interrupt is only used for measuring the interrupt service. should be removed for production
carController.prototype.eoi = function(){
/* uncomment if want to see interrupt timing
	var vEndStamp = (new Date()).getTime();
	if( this.iIPTime < (vEndStamp - this.iStamp) ){
		this.iIPTime = (vEndStamp - this.iStamp);
		console.log("new ip time " + this.iIPTime + "at counter" + this.iIPCounter);
	}
	this.iIPCounter++;
	*/
}


carController.prototype.updateThrottle = function( data, aTick ){
	if( aTick === undefined ){
		return;
	}
	if( this.iCar.ctl.blackBox.dpt[aTick] === 0 ){
		// move forward always
		this.iThrottle = 1;
		return;
	}
	if( (aTick < 15)&&(this.firstRun === true) ){
		this.iThrottle = 1;
	}else if((aTick < 30)&&(this.firstRun === true)){
		this.iThrottle = 0;
	}else{
		// do the lane change command if any
		if( this.iTrackAnalyzer.iLaneProgram.length > 0 ){
			//console.log("raw contains " +this.iCar.ctl.blackBox.raw.length + "tick:"+aTick);
			if( this.iTrackAnalyzer.iLaneProgram[0].p === this.iCar.ctl.blackBox.raw[aTick].piecePosition.pieceIndex ){
				this.iTurnMsg = {"msgType": "switchLane", "data": this.iTrackAnalyzer.iLaneProgram[0].l};
				this.iTrackAnalyzer.iLaneProgram.splice(0,1);
			}
		}
				
		var aTask = this.iTrackAnalyzer.getCurrentTask(data.piecePosition);
		if( undefined !== aTask ){
			this.iThrottle = aTask.getNextThrottle(this.iCar.ctl.blackBox, aTick, this.iTrackAnalyzer);
			
						// {distance, speed}
			var upcomingTask = this.iTrackAnalyzer.nextTask;
			//console.log("upcomming task>" + JSON.stringify(upcomingTask.start));
			var breakingRatio = this.iTrackAnalyzer.iRecordAcc.dstRatio;
			var tmpData = {piecePosition:{pieceIndex:upcomingTask.start.idx, inPieceDistance:upcomingTask.start.pos}};
			var distance = this.iTrackAnalyzer.diffDistance(data, tmpData, []);
			//console.log("Distance to next task " + distance);
			this.iCar.ctl.blackBox.taskDst[aTick] = distance;
			this.iCar.ctl.blackBox.taskLimit[aTick] = upcomingTask.data.speed;
			// accounding for turbo?
	/*		if( (aTick > this.iTrackAnalyzer.iTurboOn) && (aTick < this.iTrackAnalyzer.iTurboOff) ){
				console.log("accounting for turbo in breaking ratio "+ breakingRatio);
				breakingRatio = breakingRatio * this.iTrackAnalyzer.iTurboFactor;
				console.log("set to "+ breakingRatio + " st " + this.iTrackAnalyzer.iTurboOn + " en " + this.iTrackAnalyzer.iTurboOff);
			}else{
					console.log("Our br is "+ breakingRatio);
			}*/
			if( (breakingRatio === undefined)||((this.iCar.ctl.blackBox.dpt[aTick] - upcomingTask.data.speed)*breakingRatio ) < distance){
				// do nothing, follow own throttle
			}else{
				// need to break for the next task.
				this.iThrottle = 0;
			}
			//vfix console.log("str: "+aTick+":"+this.iThrottle);
		}else{
			// {distance, speed}
			var upcomingTask = this.iTrackAnalyzer.nextTask;
			//console.log("upcomming task>" + JSON.stringify(upcomingTask.start));
			var breakingRatio = this.iTrackAnalyzer.iRecordAcc.dstRatio;
			var tmpData = {piecePosition:{pieceIndex:upcomingTask.start.idx, inPieceDistance:upcomingTask.start.pos}};
			var distance = this.iTrackAnalyzer.diffDistance(data, tmpData, []);
			//console.log("Distance to next task " + distance);
			this.iCar.ctl.blackBox.taskDst[aTick] = distance;
			this.iCar.ctl.blackBox.taskLimit[aTick] = upcomingTask.data.speed;
			if( (aTick > this.iTrackAnalyzer.iTurboOn) && (aTick < this.iTrackAnalyzer.iTurboOff)){
				//vfix console.log("accounting for turbo in breaking ratio "+ breakingRatio);
				//breakingRatio = breakingRatio * this.iTrackAnalyzer.iTurboFactor;
				if((((upcomingTask.stop.idx- upcomingTask.start.idx)<=1)&&(Math.abs(upcomingTask.seg.a)>=45))||(((upcomingTask.idx- upcomingTask.idx)<=2)&&(Math.abs(upcomingTask.seg.a)>=90))){
					vfixxbreakingRatio = breakingRatio * 1.5;
				}
				//vfix console.log("set to "+ breakingRatio + " st " + this.iTrackAnalyzer.iTurboOn + " en " + this.iTrackAnalyzer.iTurboOff);
			}else{
					//vfix console.log("Our br is "+ breakingRatio);
			}

			if( (breakingRatio === undefined)||((this.iCar.ctl.blackBox.dpt[aTick] - upcomingTask.data.speed)*breakingRatio ) < distance){
				this.iThrottle = 1;	
			}else{
				//console.log("d:"+ distance+"br"+ breakingRatio + "sp:" + this.iCar.ctl.blackBox.dpt[aTick] +"est" + upcomingTask.data.speed);
				this.iThrottle = 0;
			}
		}
	}
	//vfix console.log(">>> " + aTick + ": " + this.iThrottle );
}

carController.prototype.getControlMessage = function( aTick ){
	// here we need to log critical parameters
	// and do the control action
	var vMsg = {
		  msgType: "throttle",
	  //    data: this.iCar.data.iThrottle
			data: this.iThrottle
		};

	if( this.iCar.ctl.blackBox.dpt[aTick] === 0 ){
		return vMsg;
	}
		
	if( this.iTurnMsg !== undefined ){
		vMsg = this.iTurnMsg;
		this.iTurnMsg = undefined;
	}else{
		// check if we have any turbo ready
		// turbo will be triggered on the longest straight line remaining on the track
		if( this.iTurbo.length > 0){
			if( this.doTurbo(this.iTurbo[0], aTick)){
				console.log("VR-O-O-O-O-O-O---O------M! at:" + aTick);
				vMsg = {"msgType": "turbo", "data": "VR-O-O-O-O-O-O---O------M!"};
				
				var aTD = this.iTurbo[0];
				console.log(aTD);
				this.iTrackAnalyzer.iTurboOn = aTick;
				this.iTrackAnalyzer.iTurboOff = aTick + aTD.turboDurationTicks;
				this.iTrackAnalyzer.iTurboFactor = aTD.turboFactor;
				this.iTurbo.splice(0,1);

			}
		}
	}
	
	return vMsg;
}


carController.prototype.doTurbo = function( data , aTick){
	if(this.iCar.ctl.blackBox.raw[aTick]!== undefined){
		//console.log(aTick + "***LAPs remaining " + (this.iRaceSession.laps - this.iCar.ctl.blackBox.raw[aTick].piecePosition.lap));
		
		if( this.turboTick <= aTick ){
			// only if the previous turbo has run out
			// if position on the loop is good
			//GLOBAL.dbg(5, "TT " + this.iTrackAnalyzer.iTurboMap[0].start +":" +this.iCar.ctl.blackBox.pi[aTick]);
			
			if( this.iTrackAnalyzer.iTurboMap.length === 0){
				//console.log("WARNING: no straight lines detected. dont use turbo!");
				//this.turboTick = aTick + data.turboDurationTicks;
				return false;							
			}
			if(this.iTrackAnalyzer.iData.pieces[this.iCar.ctl.blackBox.raw[aTick].piecePosition.pieceIndex].angle!== undefined ){
				// to make sure
				//console.log("dont want to turbo on turn");
				return false;
			}
			if( (this.iTrackAnalyzer.iTurboMap[0] !== undefined )&& (this.iTrackAnalyzer.iTurboMap[0].start === this.iCar.ctl.blackBox.pi[aTick] )){
				//console.log("We are at correct position for max turbo! " + this.iCar.ctl.blackBox.pi[aTick]);
				this.turboOn(aTick, data.turboDurationTicks, data.turboFactor );
				return true;				
			}else{
				//GLOBAL.dbg(2,"See if we can do the turbo anyways");
				var extraTurbo = this.iTurbo.length - (this.iRaceSession.laps - this.iCar.ctl.blackBox.raw[aTick].piecePosition.lap);
				GLOBAL.dbg(5,"extra " + extraTurbo);
				if( extraTurbo > 0 ){
				GLOBAL.dbg(2, "TT2 " + this.iTrackAnalyzer.iTurboMap[1].start +":" +this.iCar.ctl.blackBox.pi[aTick]);
				GLOBAL.dbg(2, "TT3 " + this.iTrackAnalyzer.iTurboMap[2].start +":" +this.iCar.ctl.blackBox.pi[aTick]);
					if (( (this.iTrackAnalyzer.iTurboMap[1] !== undefined )&& (this.iTrackAnalyzer.iTurboMap[1].start === this.iCar.ctl.blackBox.pi[aTick] ) ) ||
						((this.iTrackAnalyzer.iTurboMap[2] !== undefined )&& (this.iTrackAnalyzer.iTurboMap[2].start === this.iCar.ctl.blackBox.pi[aTick] ) )){
				GLOBAL.dbg(2, "TT2 " + this.iTrackAnalyzer.iTurboMap[1].start +":" +this.iCar.ctl.blackBox.pi[aTick]);
				GLOBAL.dbg(2, "TT3 " + this.iTrackAnalyzer.iTurboMap[2].start +":" +this.iCar.ctl.blackBox.pi[aTick]);
						//console.log("Using secondary place for turbo");
						this.turboOn(aTick, data.turboDurationTicks, data.turboFactor );
						return true;				
						
					}
				}
			}
		}
	}
	return false;
}


carController.prototype.turboOn = function(tick, duration, factor){
	this.turboTick = tick + duration;
	this.iTrackAnalyzer.iTurboOn = tick;
	this.iTrackAnalyzer.iTurboFactor = factor;
	this.iTrackAnalyzer.iTurboOff = tick + duration;
}
exports.carController = carController;