var net = require("net");
var JSONStream = require('JSONStream');

var serverHost = process.argv[2];
var serverPort = process.argv[3];
var botName = process.argv[4];
var botKey = process.argv[5];
 

GLOBAL.DEBUG_LEVEL = 0; // 7 - most info 0 - very little info 
GLOBAL.DEBUG_TESTS = false;
GLOBAL.DEBUG_TESTS_ONLY = false; 

GLOBAL.dbg = function( level, str ){
	if( GLOBAL.DEBUG_LEVEL >= level ){
		console.log(str);
	}
}
/*
if( GLOBAL.DEBUG_TESTS === true ){
	var testTrackData = require("./tests/test_track_data.js");
	var testTrackAnalyzerSuit = require("./tests/test_TrackAnalyzer.js");
	var result = true;
	result = result&&testTrackAnalyzerSuit.Suit.run(testTrackData.testTrackData);
	if(result===false){
		throw "SOME TESTS FAILED";
		}else{
			console.log("Unit Tests have passed");
		}
}

if( GLOBAL.DEBUG_TESTS_ONLY === true ){
	throw "UNIT TEST RUN ENDED";
} 
*/
var mCtl = require("./carController.js"); 
var vCtl = new mCtl.carController();
vCtl.init({}); 
console.log("I'm", botName, "and connect to", serverHost + ":" + serverPort);

client = net.connect(serverPort, serverHost, function() {
  return send({
    msgType: "join",
    data: {
      name: botName,
      key: botKey
    }
  });
  /*return send({
    "msgType": "createRace", "data": {
	"botId": {
    "name": botName,
    "key": botKey
  },
  "trackName": "germany",
  //"trackName": "france",
  //"trackName": "usa",
  //"password": "schumi4ever",
  "carCount": 1
	}
  });*/
});

function send(json,tick) {
  //json.gameTick = tick;
  client.write(JSON.stringify(json));
  return client.write('\n');
};

jsonStream = client.pipe(JSONStream.parse());

jsonStream.on('data', function(data) {
	vCtl.soi(data);  
  if (data.msgType === 'carPositions') {
	var msg = vCtl.getControlMessage(data.gameTick);
	if( {} === msg ){
		ping(data.gameTick);
	}else{
		send(msg, data.gameTick);
	}
  } else {
    if (data.msgType === 'join') {
      console.log('Joined')
    } else if (data.msgType === 'gameStart') {
      console.log('Race started');
    } else if (data.msgType === 'gameEnd') {
      console.log('Race ended');
    } 
	ping(data.gameTick);
  }
    vCtl.eoi(); 
});

function ping(tick){
    send({
      msgType: "ping",
      data: {}/*,
	  gameTick:tick*/
    });
}

jsonStream.on('error', function() {
  return console.log("disconnected");
});
