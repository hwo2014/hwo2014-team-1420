
function SingleCar( data ){
	this.rawCarData = data;
	this.tick = 0;
	
	this.initBlackBox();
}

SingleCar.prototype.initBlackBox = function(){
	this.blackBox={
		raw:[],
		dpt:[],
		acc:[],
		accChange:[],
		angle:[],
		angleChange:[],
		angleChangeR:[],
		taskDst:[],
		taskLimit:[],
		pi:[],
		pp:[],
		pio:[],
		ppo:[],
		it:[]
	}

}

SingleCar.prototype.updateCar = function(data, tick, trackAnalyzer){
	if( tick === undefined ){
		if( this.tick === 0 ){
			// initial positioning - reset 
			tick = 0;
		}else{
			//perhaps final data. do nothing for now
		}
	}else{
		if( this.tick !== ( tick - 1 )){
			GLOBAL.dbg(1,"Tick out of sync (" + this.tick + "/" + tick +")");
		}else{
		
		}
	}
	this.tick = tick;
	// here we potential want to do tick checks etc.
	
	// lets calculate parameters
	this.blackBox.raw[tick] = data;
	if( tick === 0 ){
	}else{
		//console.log("raw input:" + JSON.stringify(this.blackBox.raw[tick], null, 4));
		var laneIds=[this.blackBox.raw[tick].piecePosition.lane.endLaneIndex];
		if( this.blackBox.raw[tick-1] !== undefined  ){
			if( this.blackBox.raw[tick-1].piecePosition.pieceIndex !== this.blackBox.raw[tick].piecePosition.pieceIndex ){
				laneIds=[this.blackBox.raw[tick-1].piecePosition.lane.endLaneIndex, this.blackBox.raw[tick].piecePosition.lane.endLaneIndex];
			}
		}
		this.blackBox.dpt[tick] = trackAnalyzer.diffDistance(this.blackBox.raw[tick-1],this.blackBox.raw[tick], laneIds /* lane */);

		
		
		this.blackBox.pi[tick] = this.blackBox.raw[tick].piecePosition.pieceIndex ;
		this.blackBox.pp[tick] = this.blackBox.raw[tick].piecePosition.inPieceDistance ;
		this.blackBox.it[tick] = tick;
		if( this.blackBox.raw[tick-1] !== undefined ){
			this.blackBox.pio[tick] = this.blackBox.raw[tick-1].piecePosition.pieceIndex;
			this.blackBox.ppo[tick] = this.blackBox.raw[tick-1].piecePosition.inPieceDistance;
			}
		// ignore the data if we change the piece index
		if( this.blackBox.pi[tick] !== this.blackBox.pio[tick] ){
			this.blackBox.dpt[tick] = this.blackBox.dpt[tick-1] + this.blackBox.acc[tick-1] - this.blackBox.accChange[tick-1];		
		}


		// there are some items such as lane changing, that will effect the length calculation. atm, it seems reasonable to ignore
		// the error that they createElement
		
		this.blackBox.acc[tick]=this.blackBox.dpt[tick] - this.blackBox.dpt[tick-1];
		this.blackBox.accChange[tick]=this.blackBox.acc[tick] - this.blackBox.acc[tick-1];		
		
		this.blackBox.angle[tick] = this.blackBox.raw[tick].angle;
		this.blackBox.angleChange[tick] = this.blackBox.angle[tick] - this.blackBox.angle[tick-1];
		this.blackBox.angleChangeR[tick] = this.blackBox.angleChange[tick] - this.blackBox.angleChange[tick-1];
/*		
		GLOBAL.dbg(3,"Current DpT " + this.blackBox.dpt[tick]);		
		GLOBAL.dbg(3,"Current acc " + this.blackBox.acc[tick]);
		GLOBAL.dbg(3,"Current accChange " + this.blackBox.accChange[tick]);
		GLOBAL.dbg(3,"Current angle " + this.blackBox.angle[tick]);
		GLOBAL.dbg(3,"Current angleChange " + this.blackBox.angleChange[tick]);
		GLOBAL.dbg(3,"Current next task dst " + this.blackBox.taskDst[tick]);
		GLOBAL.dbg(3,"Current next task speed " + this.blackBox.taskLimit[tick]);
	
		GLOBAL.dbg(3,"Current acc record" + JSON.stringify(trackAnalyzer.iRecordAcc, null, 4));
*/	
		trackAnalyzer.recordAcc(this.blackBox.dpt[tick],this.blackBox.acc[tick], tick);
	}
}

SingleCar.prototype.dumpBlackBox = function(){
	for(var i in this.blackBox.raw){
		GLOBAL.dbg(1, " " + i + 
			" ; " + this.blackBox.dpt[i] + 
			" ; " + this.blackBox.acc[i] +
			" ; " + this.blackBox.accChange[i] + 			
			" ; " + this.blackBox.angle[i] + 			
			" ; " + this.blackBox.angleChange[i] +
			" ; " + this.blackBox.angleChangeR[i] +
			" ; " + this.blackBox.taskDst[i] +
			" ; " + this.blackBox.taskLimit[i] +
			" ; " + this.blackBox.pi[i] +
			" ; " + this.blackBox.pp[i] + 
			" ; " + this.blackBox.it[i] +
			" ; " + this.blackBox.pio[i] + 
			" ; " + this.blackBox.ppo[i] 
			);
	}
}
	



exports.SingleCar = SingleCar;